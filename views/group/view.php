<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_of_group',
            'code',
            'specialty:ntext',
            [
                'label' => 'Студенты',
                'format' => 'raw',
                'value' => function ($model) {
                    $stud = '';
                    $students = \app\models\Students::findAll(['id_group' => $model->id]);
                    foreach ($students as $student) {
                        $stud .= '<li>' . $student->name . '</li>';
                    }
                    return $stud;
                }
            ],
        ],
    ]) ?>

</div>
