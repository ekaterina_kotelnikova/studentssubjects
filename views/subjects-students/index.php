<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubjectsStudentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subjects Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-students-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subjects Students', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'id_student',
                'label' => 'Имя студента',
                'value' => function ($m) {
                    /* @var $m \app\models\SubjectsStudents */
                    return \app\models\Students::findOne(['id' => $m->id_student])->name;
                }
            ],
            'id_subject',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
