<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SubjectsStudents */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Subjects Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-students-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_subject',
            [
                'attribute' => 'id_student',
                'value' => function ($model) {
                    /* @var $model \app\models\SubjectsStudents*/
                    return \app\models\Students::findOne(['id'=> $model->id_student])->name;

                }
            ],
        ],
    ]) ?>

</div>
