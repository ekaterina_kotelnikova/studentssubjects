<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Students */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Студенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            //'id_group',
            [
                'label' => 'Группа',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model \app\models\Students  */
                    return $model->group->name_of_group;

                }
            ],
            [
                'label' => 'Предметы',
                'format' => 'raw',
                'value' =>
                    function ($model) {

                        $subj = '';
                        foreach ($model->links as $link) {
                            /* @var $link \app\models\SubjectsStudents */
                            $subj .= '<li>' . \app\models\Subjects::findOne(['id' => $link->id_subject])->name . '</li>';
                        }
                        return $subj;
                    },

            ],

        ],


    ]) ?>


    <!--    --><? //=
    //    function ($model) {
    //    foreach ($model->links as $link) {
    //        /* @var $link \app\models\SubjectsStudents */
    //        echo \app\models\Subjects::findOne(['id' => $link->id_subject])->name;
    //
    //    }
    //
    //
    //    ?>


</div>
