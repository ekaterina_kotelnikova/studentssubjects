<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Students */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="students-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'id_group')->DropDownList(\yii\helpers\ArrayHelper::map(\app\models\Group::find()->all(),'id','name_of_group'))->label('Группа');?>

    <?= $form->field($model, 'subjectsBuf')->checkboxList(\yii\helpers\ArrayHelper::map(\app\models\Subjects::find()->all(),'id','name'))->label('Предметы');?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
