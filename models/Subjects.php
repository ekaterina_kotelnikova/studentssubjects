<?php

namespace app\models;


/**
 * This is the model class for table "subjects".
 *
 * @property int $id
 * @property string $name
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_subject' => 'ID предмета',
            'name' => 'Название предмета',
        ];
    }
}
