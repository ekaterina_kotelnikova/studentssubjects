<?php

namespace app\models;


/**
 * This is the model class for table "subjects-students".
 *
 * @property int $id
 * @property int $id_student
 * @property int $id_subject
 *
 * @property Subjects $subject
 * @property Students $student
 *
 */
class SubjectsStudents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects_to_students';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_student', 'id_subject'], 'required'],
            [['id_student', 'id_subject'], 'integer'],
        ];
    }

    public function getStudent() {
        $this->hasOne(Students::class,['id' => 'id_student']);
    }

    public function getSubject() {
        $this->hasOne(Subjects::class,['id' => 'id_subject']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_student' => 'Id Student',
            'id_subject' => 'Id Subject',
        ];
    }
}
