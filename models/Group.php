<?php

namespace app\models;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name_of_group
 * @property string $specialty
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_of_group'], 'string'],
            [['code'], 'integer'],
            [['specialty'], 'string'],
        ];
    }

    public function getStudents() {
        $this->hasMany(Students::class,['id' => 'id_group']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_of_group' => 'Номер группы',
            'code' => 'Код специльности',
            'specialty' => 'Название специальности',
        ];
    }
}
