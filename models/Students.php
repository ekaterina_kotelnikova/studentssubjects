<?php

namespace app\models;

/**
 * This is the model class for table "students".
 *
 * @property int id
 * @property string $name
 *
 * @property Group $group
 */
class Students extends \yii\db\ActiveRecord
{

    public $subjectsBuf;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['id_group'], 'integer'],
            ['subjectsBuf', 'safe']
        ];
    }

    public function getLinks()
    {
        return $this->hasMany(SubjectsStudents::class, ['id_student' => 'id']);
    }

    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'id_group']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_students' => 'ID студента',
            'name' => 'Имя',
            'id_group' => 'Группа',
        ];
    }


}
