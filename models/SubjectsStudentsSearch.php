<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SubjectsStudentsSearch represents the model behind the search form of `app\models\SubjectsStudents`.
 */
class SubjectsStudentsSearch extends SubjectsStudents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_student', 'id_subject'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubjectsStudents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'id_students' => $this->id_students,
//            'id_subjects' => $this->id_subjects,
//        ]);

        return $dataProvider;
    }
}
